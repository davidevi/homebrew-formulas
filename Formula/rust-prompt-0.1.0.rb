class RustPrompt010 < Formula
  version "0.1.0"
  desc "A simple command-line prompt for OpenAI's ChatGPT"
  homepage "https://gitlab.com/davidevi/rust-prompt"
  url "https://gitlab.com/davidevi/rust-prompt/-/archive/#{version}/rust-prompt-#{version}.tar.gz"
  sha256 "6053f902b9edd96f61d92f90a97d8d4958a9db3f69a9166fb8290091a599c6f4"

  license "MIT"

  depends_on "rust" => :build

  def install
    system "cargo", "install", *std_cargo_args
    bin.install_symlink "rust-prompt" => "rp"
  end

  test do
    # Add a simple test if applicable
    assert_match "rust-prompt #{version}", shell_output("#{bin}/rust-prompt --version")
  end
end

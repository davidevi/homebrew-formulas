class RustPrompt011 < Formula
  version "0.1.1"
  desc "A simple command-line prompt for OpenAI's ChatGPT"
  homepage "https://gitlab.com/davidevi/rust-prompt"
  url "https://gitlab.com/davidevi/rust-prompt/-/archive/#{version}/rust-prompt-#{version}.tar.gz"
  sha256 "63b6cb224308d56bbbaedd2e918687f212838ccd8f5e9688f3f73be10ce897d5"

  license "MIT"

  depends_on "rust" => :build

  def install
    system "cargo", "install", *std_cargo_args
    bin.install_symlink "rust-prompt" => "rp"
  end

  test do
    # Add a simple test if applicable
    assert_match "rust-prompt #{version}", shell_output("#{bin}/rust-prompt --version")
  end
end

class RustPrompt0016 < Formula
  version "0.0.16"
  desc "A simple command-line prompt for OpenAI's ChatGPT"
  homepage "https://gitlab.com/davidevi/rust-prompt"
  url "https://gitlab.com/davidevi/rust-prompt/-/archive/#{version}/rust-prompt-#{version}.tar.gz"
  sha256 "7d1d48a6baa9d8d8827aa2477ff52f5a242db0e248ccec498bd270038deb52c1"

  license "MIT"

  depends_on "rust" => :build

  def install
    system "cargo", "install", *std_cargo_args
    bin.install_symlink "rust-prompt" => "rp"
  end

  test do
    # Add a simple test if applicable
    assert_match "rust-prompt #{version}", shell_output("#{bin}/rust-prompt --version")
  end
end

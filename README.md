# Davide's Homebrew Formulas

Originally created for the Rust Prompt app

Tap it
```shell
brew tap davidevi/homebrew-formulas https://gitlab.com/davidevi/homebrew-formulas.git
```

Install from it
```shell
brew install davidevi/homebrew-formulas/rust-prompt
```
